/// <reference types="cypress"/>

import { wordpressMenu } from '../../selectors/login-menu';

export const loginResult = {
  enterCredentials: (loginObject) => {
    cy.wait(1000).get(wordpressMenu.user).type(loginObject.username);
    cy.get(wordpressMenu.user_psw).type(loginObject.psw);
    cy.get(wordpressMenu.loginButton).click();
  },

  uiLogin: function (loginObject) {
    this.enterCredentials(loginObject);
    cy.get(wordpressMenu.wpTitle).should('contain.text', 'Welcome to WordPress!');
  },

  loginUserFail: function (loginObject) {
    this.enterCredentials(loginObject);
    cy.get(wordpressMenu.loginError).should('contain.text', 'Unknown username. Check again or try your email address.');
  },

  loginPswFail: function (loginObject) {
    this.enterCredentials(loginObject);
    cy.get(wordpressMenu.loginError).should(
      'contain.text',
      'Error: The password you entered for the username automation is incorrect. Lost your password?'
    );
  },

  loginEmptyCredentials: function (loginObject) {
    cy.get(wordpressMenu.loginButton).click();
    cy.get(wordpressMenu.loginError).should(
      'contain.text',
      'Error: The username field is empty.' || 'Error: The password field is empty.'
    );
  },
};
