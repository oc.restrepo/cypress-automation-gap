/// <reference types="cypress"/>

import { searchMenu } from '../selectors/search-menu';

export const search = {
  uiSearch: function (item) {
    cy.get(searchMenu.searchInput).type(item + '{enter}');
  },

  uiValidSearch: function (item) {
    this.uiSearch(item);
    cy.get(searchMenu.productTtle).should('have.text', item);
  },

  uiInvalidSearch: function (item) {
    this.uiSearch(item);
    cy.get(searchMenu.noProduct).should('have.text', 'No products were found matching your selection.');
  },
  emptySearch: function (item) {
    this.uiSearch(item);
    cy.get(searchMenu.nulTitle).should('have.text', 'Search results: “”');
  },
};
