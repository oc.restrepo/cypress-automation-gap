/// <reference types = "cypress" />

const { apiConst } = require('../../support/apiConsts');

describe('api login verification', () => {
  it('should login to the API - happy path', () => {
    cy.request({
      method: 'POST',
      url: '/',
      auth: { username: Cypress.env('API-USER'), password: Cypress.env('API-PSW') },
      body: apiConst.user,
    }).as('logged_user');
    cy.get('@logged_user').then((response) => {
      expect(response.status).to.eq(200);
      expect(response).to.be.ok;
    });
  });
});
