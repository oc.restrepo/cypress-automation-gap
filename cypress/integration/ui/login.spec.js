/// <reference types = "cypress" />

import { urls } from '../../support/urls';
import { wordpressMenu } from '../../selectors/login-menu';
import { loginResult } from '../../support/authentication/authentication';

describe('UI Login Validation', () => {
  beforeEach('Verify wordpress login page loads ', () => {
    cy.visit(urls.login);
    //Assertion
    cy.get(wordpressMenu.loginButton).should('be.visible').should('have.value', 'Log In');
  });

  it('should login to wordpress', () => {
    loginResult.uiLogin({ username: Cypress.env('UI-USER'), psw: Cypress.env('UI-PSW') });
  });

  it('should fail: invalid username', () => {
    loginResult.loginUserFail({ username: Cypress.env('UI-USER') + 'fake123', psw: Cypress.env('UI-PSW') });
  });

  it('should fail: invalid password', () => {
    loginResult.loginPswFail({ username: Cypress.env('UI-USER'), psw: Cypress.env('UI-PSW') + 'fake123' });
  });

  it('should fail: empty credentials', () => {
    loginResult.loginEmptyCredentials();
  });
});
