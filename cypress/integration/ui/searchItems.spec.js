/// <reference types="cypress"/>
import { mainMenu } from '../../selectors/home-menu.js';
import { search } from '../../support/search.js';
import { uiVariables } from '../../support/variables.js';

describe('top menu navigation', () => {
  beforeEach('Verify main page loads', () => {
    cy.visit('');
    //Assertion
    cy.get(mainMenu.mainTitle).should('be.visible').should('contain.text', 'Personal Styling for Everybody');
  });
  //Search Verification

  it('should search an existing product', () => {
    search.uiValidSearch(uiVariables.validSearchItem);
  });

  it('should search an invalid product', () => {
    search.uiInvalidSearch(uiVariables.validSearchItem + 'fake123');
  });

  it('should search a null product', () => {
    search.emptySearch('');
  });
});
