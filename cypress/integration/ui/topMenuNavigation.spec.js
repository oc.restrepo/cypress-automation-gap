/// <reference types = "cypress" />

import { ordersMenu } from '../../selectors/orders-menu.js';
import { mainMenu } from '../../selectors/home-menu.js';
import { cartMenu } from '../../selectors/cart-menu.js';
import { addressesMenu } from '../../selectors/addresses-menu.js';
import { downloadsMenu } from '../../selectors/downloads-menu';
import { lostPswHome } from '../../selectors/lostpsw-home';

describe('top menu navigation', () => {
  beforeEach('Verify main page loads', () => {
    cy.visit('');
    //Assertion
    cy.get(mainMenu.mainTitle).should('be.visible').should('contain.text', 'Personal Styling for Everybody');
  });
  //Home tab Verification

  it('should verify link name is equal to Home', () => {
    cy.get(mainMenu.homeTab).should('contain.text', 'Home');
  });

  it('should verify Home link navigation', () => {
    cy.get(mainMenu.homeTab).click();
  });

  //Shop tab Verification

  it('should verify link name is equal to Shop', () => {
    cy.get(mainMenu.shopTab).should('contain.text', 'Shop');
  });

  it('should verify Shop displayable list first item is equal to Cart', () => {
    cy.get(mainMenu.cart).should('contain.text', 'Cart');
  });

  it('should verify Cart link navigation', () => {
    cy.get(mainMenu.cart).click({ force: true });
    cy.get(cartMenu.cartTitle).should('contain.text', 'Your cart is currently empty.');
  });

  it('should verify Shop displayable list second item is equal to CHECKOUT', () => {
    cy.get(mainMenu.shopCheckout).should('contain.text', 'Checkout');
  });

  it('should verify Checkout link navigation', () => {
    cy.get(mainMenu.shopCheckout).click({ force: true });
    cy.get(cartMenu.cartTitle).should('contain.text', 'Your cart is currently empty.');
  });

  //Account details tab Verification

  it('should verify link name is equal to Account details', () => {
    cy.get(mainMenu.accountTab).should('contain.text', 'Account details');
  });

  it('should verify AccountDetails displayable list first item is equal to ORDERS', () => {
    cy.get(mainMenu.accountOrders).should('contain.text', 'Orders');
  });

  it('should verify Orders link navigation', () => {
    cy.get(mainMenu.accountOrders).click({ force: true });
    cy.get(ordersMenu.ordersTitle).should('contain.text', 'Orders');
  });

  it('should verify AccountDetails displayable list second item is equal to DOWNLOADS', () => {
    cy.get(mainMenu.accountDownloads).should('contain.text', 'Downloads');
  });

  it('should verify Doenloads link navigation', () => {
    cy.get(mainMenu.accountDownloads).click({ force: true });
    cy.get(downloadsMenu.downloadsTitle).should('contain.text', 'Downloads');
  });

  it('should verify AccountDetails displayable list Third item is equal to Addresses', () => {
    cy.get(mainMenu.accountAddresses).should('contain.text', 'Addresses');
  });

  it('should verify Addresses link navigation', () => {
    cy.get(mainMenu.accountAddresses).click({ force: true });
    cy.get(addressesMenu.addressesTitle).should('contain.text', 'Addresses');
  });

  it('should verify AccountDetails displayable list fourth item is equal to Lost Password', () => {
    cy.get(mainMenu.accountLostPsw).should('contain.text', 'Lost password');
  });

  it('should verify Lost password link navigation', () => {
    cy.get(mainMenu.accountLostPsw).click({ force: true });
    cy.get(lostPswHome.lostPswTitle).should('contain.text', 'Lost password');
  });
});
