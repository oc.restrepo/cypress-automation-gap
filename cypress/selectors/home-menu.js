export const mainMenu = {
    homeTab: 'li.menu-item-142 > [href *= "/"]',
    shopTab: 'li.menu-item-145 > [href*="/shop"]',
    shopCheckout: 'li.menu-item-145 li.menu-item-144 > [href *= "/checkout"]',
    accountTab: 'li.menu-item-146 > [href*="/my-account/edit-account/"]',
    accountOrders: 'li.menu-item-146 li.menu-item-150 > [href *= "/my-account/orders/"]',
    accountDownloads: 'li.menu-item-146 li.menu-item-151 > [href *= "/my-account/downloads/"]',
    accountAddresses: 'li.menu-item-146 li.menu-item-152 > [href *= "/my-account/edit-address/"]',
    accountLostPsw: 'li.menu-item-146 li.menu-item-153 > [href *= "/my-account/lost-password/"]',
    cart: 'li.menu-item-145 li.menu-item-143 > [href *= "/cart"]',
    mainTitle: '#main div.wp-block-cover__inner-container',

};
