export const wordpressMenu = {
  loginButton: '#wp-submit',
  user: '#user_login',
  user_psw: '#user_pass',
  wpTitle: '.welcome-panel-content > h2',
  loginError: '#login_error',
};
